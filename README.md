# ИНСТРУКЦИЯ

# Зависимости
- https://nodejs.org/en/
- docker
- docker-compose
- kubectl
- helm

# Локальный запуск только для разработки
- npm i
- npm run dev:env - дождаться запуска контейнеров
- npm run dev:start - запуск всех процессов
- http://localhost:9000/

# Задачи и требования
- докеризировать проект, нужны поды front, api, worker
- запушить образы в https://registry.tages.ru/demo/*  (логин demo, пароль demo)
- создать helm chart
- создать pipeline в gitlab'e для сборки образов и автодеплоя в k8s через helm
- реализовать readinessProbe для api и worker (GET /healthcheck = HTTP 200)
- helm должен запускаться с аргументом --wait и ждать успешный запуск всего окружения
- сервис должен открываться через домен https://tages-student-*.k8s-platformeco.tages.xyz/
- вся инфраструктура по проекту должна быть в репозитории
- задача считается успешной, если на чистый NS из pipeline'a идет деплой, после деплоя сервис открывается и можно там зарегистрироваться\залогиниться
